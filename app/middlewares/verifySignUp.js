const db = require("../models");
const ROLES = db.ROLES;
const User = db.user;

checkDuplicateUsernameOrEmail = async (req, res, next) => {
  try{
    const isDuplicate  = await User.findOne({username: req.body.username});
    if(isDuplicate) return res.status(400).send({ message: `Erreur! ${req.body.username} est déjâ pris!` });

    const existingEmail = await User.findOne({ email: req.body.email });
    if (existingEmail) return res.status(400).send({ message: `Erreur! ${req.body.email} est déjà utilisé!` });
    next();
  } catch(error){
    res.status(500).send({ message: error });
  }
};

checkRolesExisted = (req, res, next) => {
  if (req.body.roles) {
    for (let i = 0; i < req.body.roles.length; i++) {
      if (!ROLES.includes(req.body.roles[i])) {
        res.status(400).send({
          message: `Erreur! Le profile ${req.body.roles[i]} n'existe pas!`
        });
        return;
      }
    }
  }
  next();
};

const verifySignUp = {
    checkDuplicateUsernameOrEmail,
    checkRolesExisted
};

module.exports = verifySignUp;