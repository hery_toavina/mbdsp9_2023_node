const util = require("util");
const multer = require("multer");
const { GridFsStorage }  = require("multer-gridfs-storage");
const dbConfig = require("../config/db.config");


// Create a disk storage for local file storage
const diskStorage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "uploads/"); // Specify the destination folder where the files will be stored locally
    },
    filename: function (req, file, cb) {
      const match = ["image/png", "image/jpeg"];
      const extension = file.mimetype.split("/")[1];
  
      if (match.indexOf(file.mimetype) === -1) {
        const filename = `${Date.now()}-tpt-${file.originalname}`;
        cb(null, filename);
      } else {
        const filename = `${Date.now()}-tpt-${file.originalname}.${extension}`;
        cb(null, filename);
      }
    },
});

const DBStorage = new GridFsStorage({
  url: `mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`,
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  file: (req, file) => {
    const match = ["image/png", "image/jpeg"];

    if (match.indexOf(file.mimetype) === -1) {
      const filename = `${Date.now()}-tpt-${file.originalname}`;
      return filename;
    }

    return {
      bucketName: dbConfig.imgBucket,
      filename: `${Date.now()}-tpt-${file.originalname}`
    };
  }
});

const uploadToDB = multer({ storage: DBStorage }).single("file");
const uploadToDir = multer({ storage: diskStorage }).single("file");

const uploadToDBMiddleware = util.promisify(uploadToDB);
const uploadToDirMiddleware = util.promisify(uploadToDir);

const uploads = {
    uploadToDBMiddleware,
    uploadToDirMiddleware
};

module.exports = uploads;