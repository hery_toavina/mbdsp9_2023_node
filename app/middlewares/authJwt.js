const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;
const Role = db.role;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({ message: "Authentification requis!" });
  }

  jwt.verify(token,
            config.secret,
            (err, decoded) => {
              if (err) {
                return res.status(401).send({
                  message: "Unauthorized!",
                });
              }
              req.userId = decoded.id;
              next();
            });
};

authorize = function (roles = []) {
  if (!Array.isArray(roles)) roles = [roles];
  return async (req, res, next) => {
    let user = await User.findById(req.userId).populate("roles", "-__v");
    let userRoles = user.roles.map( r => r['name']);
    try {
      if(!roles.every(role => userRoles.includes(role))){
        res.status(403).send({ message: "Refusé! vous n'avez pas les privilèges requis!" });
      } else {
        next();
      }
    } catch (error) {
      res.status(500).send({ message: error });
    }
  };
  
}

const authJwt = {
  verifyToken,
  authorize
};
module.exports = authJwt;
