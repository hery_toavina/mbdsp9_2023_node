const authJwt = require("./authJwt");
const verifySignUp = require("./verifySignUp");
const uploads = require("./upload")

module.exports = {
  authJwt,
  verifySignUp,
  uploads
};