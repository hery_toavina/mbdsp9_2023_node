const mongoose = require("mongoose");

const feedSchema =  new mongoose.Schema({
  title: String,
  content: String,
  published:  {
      type: Boolean,
      default: true
  },
  imageName: String
},
{
  timestamps: true
})

const Feed = mongoose.model(
  "Feed",
  feedSchema
);

module.exports = Feed;