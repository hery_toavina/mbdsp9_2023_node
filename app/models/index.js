const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.feed = require("./feed.model");
db.document = require("./document.model");
db.etape = require("./etapes.model")

db.RoleNames = {
    admin: "ROLE_ADMIN",
    moderator: "ROLE_MODERATOR",
    citizen: "ROLE_CITIZEN"
} 

db.ROLES = [db.RoleNames.admin, db.RoleNames.moderator, db.RoleNames.citizen];

db.RolesArray = {
    citizen: [db.RoleNames.citizen],
    admin: [db.RoleNames.admin],
    moderator: [db.RoleNames.moderator],
    all: [db.RoleNames.admin, db.RoleNames.moderator, db.RoleNames.citizen]
};

module.exports = db;