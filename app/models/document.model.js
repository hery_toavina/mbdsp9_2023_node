const mongoose = require('mongoose');

const documentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  keywords: {
    type: String,
    required: true
  },
  processing_time: {
    type: Number,
    required: true
  },
  cost: {
    type: Number,
    required: true
  },
  steps: {
    type: String,
    required: true
  }
},
{
  timestamps: true
});

const Document = mongoose.model('Document', documentSchema);

module.exports = Document;
