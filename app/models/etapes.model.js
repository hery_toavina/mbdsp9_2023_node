const mongoose = require('mongoose');

const stepSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  order: {
    type: Number,
    required: true
  }
},
{
  timestamps: true
});

const Step = mongoose.model('Step', stepSchema);

module.exports = Step;
