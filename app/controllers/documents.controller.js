const db = require("../models");
const Document = db.document;
const Etape = db.etape;


exports.create = async (req, res) => {
  const { name, description, keywords, processing_time, cost, steps } = req.body;

  Document.create({
    name,
    description,
    keywords,
    processing_time,
    cost,
    steps
  })
    .then(doc => {
      res.status(201).json(doc);
    })
    .catch(err => {
      res.status(500).json({ message: err });
    });
}

exports.findOne = async (req, res) => {
    const documentId = req.params.id;
  
    Document.findById(documentId).populate('steps').exec()
      .then(document => {
        if (!document) {
          return res.status(404).json({ message: 'Document introuvable' });
        }
  
        res.json(document);
      })
      .catch(err => {
        console.error('Erreur lors de la récupération du document :', err);
        res.status(500).json({ message: 'Erreur serveur lors de la récupération du document' });
      });
  }

exports.searchByKeyword = async (req, res) => {
  
  if(!req.query.keyword) { Document.find({}).then(resultatDocuments => {
    res.json(resultatDocuments);
  })
} else {
   const searchTerm = req.query.keyword.toLowerCase();
   Document.find({ keywords: { $regex: searchTerm, $options: 'i' } })
     .then(resultatDocuments => {
       res.json(resultatDocuments);
     })
     .catch(err => {
       res.status(500).json({ message: err });
     });
 }
 
}


 exports.update = async (req, res) => {
  try {
      const id = req.params.id;
      await Document.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
      res.send({ message: "Document mis à jour!"});
  } catch (error) {
      res.status(500).send({ message: error});
  }
};