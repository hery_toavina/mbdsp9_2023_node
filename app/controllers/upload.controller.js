const { uploads } = require("../middlewares");
const dbConfig = require("../config/db.config");
const db = require("../models");
const mongoose = db.mongoose;

const MongoClient = require("mongodb").MongoClient;
// const { ObjectID } = require("mongodb");

const url = `mongodb://${dbConfig.HOST}:${dbConfig.PORT}/`;

const mongoClient = new MongoClient(url);

const uploadFiles = async (req, res) => {
  try {
  //  await uploads.uploadToDirMiddleware(req, res);
    await uploads.uploadToDBMiddleware(req, res);
    console.log(req.file);

    if (req.file == undefined) {
      return res.send({
        message: "You must select a file.",
      });
    }

    return res.send({
      message: "File has been uploaded.",
    });
  } catch (error) {
    console.log(error);

    return res.send({
      message: `Error when trying upload image: ${error}`,
    });
  }
};

const getListFiles = async (req, res) => {
  try {
    await mongoClient.connect();

    const database = mongoClient.db(dbConfig.DB);
    const images = database.collection(dbConfig.imgBucket + ".files");

    const cursor = images.find({});
    const count = await images.countDocuments();

    if (count === 0) {
      return res.status(500).send({
        message: "No files found!",
      });
    }

    const fileInfos = await cursor.toArray(); // Convert the cursor to an array

    const processedFiles = fileInfos.map((doc) => ({
      name: doc.filename,
    }));

    return res.status(200).send(processedFiles);
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

const display = async (req, res) => {
    try {
        const filename = req.params.filename;
        mongoose.connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, { useNewUrlParser: true, useUnifiedTopology: true });
        const db = mongoose.connection;
        const bucket = new mongoose.mongo.GridFSBucket(db.db, {bucketName : dbConfig.imgBucket});
        const image = await db.collection(dbConfig.imgBucket + ".files").findOne({ filename });
      //  console.log(image);
        if(!image){
            return res.status(404).send({
                message: 'Image introuvable!',
            });
        }
        const downloadStream = bucket.openDownloadStreamByName(image.filename);
      
        res.set('Content-Type', image.contentType); // Modify this based on your image type
      
        // Pipe the GridFS file stream to the response
        downloadStream.pipe(res);
        
    } catch (error) {
        return res.status(500).send({
            message: error,
        });
    }
};

module.exports = {
  uploadFiles,
  getListFiles,
  display,
};