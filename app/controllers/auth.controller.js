const config = require("../config/auth.config");
const db = require("../models");
const User = db.user
const Role = db.role;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.register = async (req, res) => {
  const user = new User({
    username: req.body.username,
    email: req.body.email.trim(),
    password: bcrypt.hashSync(req.body.password, 8)
  });

  try {
    if (req.body.roles) {
      const roles = await Role.find({ name: { $in: req.body.roles }});
      user.roles = roles.map(role => role._id);
    } else {
      const role = await Role.findOne({ name: db.RoleNames.citizen });
      user.roles = [role._id];
    }
    await user.save();
    res.send({ message: "Nouvel Utilisateur ajouté!" });
  } catch (error) {
    res.status(500).send({ message: error });
  }
};

exports.login = async (req, res) => {
  try {
    const user = await User.findOne({
      email: req.body.email.trim()
    })
      .populate("roles", "-__v");
  
      if (!user) {
        return res.status(404).send({ message: "Utilisateur inexistant." });
      }
  
      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );
  
      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Mot de passe invalide!"
        });
      }
  
        const token = jwt.sign({ id: user.id },
                                config.secret,
                                {
                                  algorithm: 'HS256',
                                  allowInsecureKeySizes: true,
                                  expiresIn: 86400, // 24 hours
                                });
  
        var authorities = user.roles;
  
    /*    for (let i = 0; i < user.roles.length; i++) {
          uthorities.push("ROLE_" + user.roles[i].name.toUpperCase());
        } */
        res.status(200).send({
          id: user._id,
          username: user.username,
          email: user.email,
          roles: authorities,
          accessToken: token
        });
  } catch (error) {
    res.status(500).send({ message: error });
  }
};