const db = require("../models");
const Feed = db.feed;

exports.create = async (req, res) => {
  const feed = new Feed({
    title: req.body.title,
    content: req.body.content,
    published: req.body.published,
    imageName: req.body.imageName
  });

  try {
    await feed.save();
    res.send({ message: "Nouvel actualités publié!"});
  } catch (error) {
    res.status(500).send({ message: error});
  }

};

exports.findAll = async (req, res) => {
  const feeds = await Feed.find({});
  try {
      if(feeds) res.send(feeds);
  } catch (error) {
    res.status(500).send({ message: error});
  }
};

exports.findOne = async (req, res) => {
    const id = req.params.id;
    try {
        if(id){
            const feed = await Feed.findById(id);
            res.send(feed);
        }
    } catch (error) {
        res.status(500).send({ message: error});
    }
};

exports.update = async (req, res) => {
    try {
        const id = req.params.id;
        await Feed.findByIdAndUpdate(id, req.body, { useFindAndModify: false });
        res.send({ message: "Actualités mis à jour!"});
    } catch (error) {
        res.status(500).send({ message: error});
    }
};

exports.delete = async (req, res) => {
  try {
    await Feed.findByIdAndRemove(id)
    res.send({ message: "Actualités suprrimé!"});
  } catch (error) {
    res.status(500).send({ message: error});
  }
};

exports.findAllPublished = async (req, res) => {
  const feeds = await Feed.find({published: true});
  res.send(feeds);
};