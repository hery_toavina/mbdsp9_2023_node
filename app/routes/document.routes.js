const doc = require("../controllers/documents.controller");
const { authJwt } = require("../middlewares");
const db = require("../models");

module.exports = app => {
  
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    
    var router = require("express").Router();
  
    router.post("/", [authJwt.verifyToken], doc.create);
    router.get("/search", doc.searchByKeyword);
    router.get("/:id", doc.findOne);
    router.put("/:id", [authJwt.verifyToken, authorize(db.RolesArray.moderator)], doc.update);
    app.use('/api/doc', router);
  };
  