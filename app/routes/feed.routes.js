const feed = require("../controllers/feed.controller");
const { authJwt } = require("../middlewares");
const db = require("../models");

module.exports = app => {
  
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    
    var router = require("express").Router();
  
    router.post("/", [authJwt.verifyToken, authorize(db.RolesArray.moderator)], feed.create);
  
    router.get("/", [authJwt.verifyToken, authorize(db.RolesArray.moderator)], feed.findAll);
  
    router.get("/published",feed.findAllPublished);
  
    router.get("/:id",/* [authJwt.verifyToken, authorize(db.RolesArray.moderator)],*/ feed.findOne);
  
    router.put("/:id", [authJwt.verifyToken, authorize(db.RolesArray.moderator)], feed.update);
  
    router.delete("/:id", [authJwt.verifyToken, authorize(db.RolesArray.moderator)], feed.delete);
    
    app.use('/api/feed', router);
  };
  